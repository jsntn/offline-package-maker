# GitLab CI/CD Pipeline for Package Compression

Take `sshpass` as example here, change it as your case may be.

This GitLab CI/CD pipeline does the following:

1. Uses an Ubuntu 16.04 Docker image
2. Installs the `sshpass` package on the image
3. Downloads the `sshpass` Debian package files without installing 
4. Copies the package files from `/var/cache/apt/archives` to the GitLab project directory
5. Creates a `package` stage to compress the package files
6. Creates a `sshpass` directory and copies the package files into it
7. Compresses the `sshpass` directory into a `sshpass.tar.gz` file 
8. Calculates the MD5 hash of the compressed file
9. Uploads the compressed file as an artifact which expires in 30 minutes
10. Tags the job with `docker`

The end result is a compressed `sshpass.tar.gz` file which contains the Debian packages required to install `sshpass`. This can then be downloaded and extracted on another Ubuntu 16.04 system to install the `sshpass` package.